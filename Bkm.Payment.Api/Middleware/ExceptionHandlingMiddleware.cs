﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Bkm.Payment.Dto.Response;
using Bkm.Payment.Exception;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Bkm.Payment.Api
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, System.Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var response = new ResponseBase()
            {
                Success = false
            };

            if (exception is BusinessRuleException)
            {
                var ruleException = (BusinessRuleException)exception;

                response.Message = ruleException.Message;
                response.UserMessage = ruleException.UserMessage;
                response.Code = (int)ruleException.Code;

                code = HttpStatusCode.BadRequest;
            }
            else
            {
                response.Message = exception.Message;
                response.Code = (int)ErrorCode.InternalServerError;
            }

            var result = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ExceptionHandlingMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandlingMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlingMiddleware>();
        }
    }
   
}
