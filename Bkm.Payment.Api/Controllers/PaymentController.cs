﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bkm.Payment.ApplicationService;
using Bkm.Payment.Dto.Request;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Bkm.Payment.Api.Controllers
{
    [Route("api/[controller]")]
    public class PaymentController : Controller
    {
        private readonly IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService){
            _paymentService = paymentService;
        }
        [HttpPost]
        [Route("pay")]
        public ObjectResult Pay(PaymentPayRequest request){

            var response = _paymentService.Pay(request);
            return Ok(response);
        }
        // GET: api/values
        [HttpGet]
        public ObjectResult Get(Guid paymentId)
        {
            var response = _paymentService.GetPayment(paymentId);
            return Ok(response);
        }
    }
}
