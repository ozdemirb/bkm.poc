﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bkm.Payment.ApplicationService;
using Bkm.Payment.Domain;
using Bkm.Payment.Repository;
using Bkm.Payment.Repository.Oracle;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace Bkm.Payment.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = "server=paymentdbinstance.cf1od9nt0c7n.us-east-2.rds.amazonaws.com;user id=admin;password=admin12345;database=BkmPayment";

            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Account.Api", Version = "v1" });
            });
            //Migration Assembly Code First yapısını destekliyor;
            services.AddDbContext<PaymentDbContext>(options => options.UseMySQL(connectionString, b => b.MigrationsAssembly("Account.Api")));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPaymentManager, PaymentManager>();
            services.AddTransient<IPaymentRepository,Bkm.Payment.Repository.PaymentRepository>();
            services.AddTransient<IPaymentOracleRepository, Payment.Repository.Oracle.PaymentRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseMiddleware(typeof(ExceptionHandlingMiddleware));
            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My Customer API V1");
            });
        }
    }
}
