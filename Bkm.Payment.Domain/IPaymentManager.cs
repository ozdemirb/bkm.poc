﻿using System;
namespace Bkm.Payment.Domain
{
    public interface IPaymentManager
    {   
        string SavePayment(PaymentBase payment);

        PaymentBase GetPayment(Guid PaymentId);
    }
}
