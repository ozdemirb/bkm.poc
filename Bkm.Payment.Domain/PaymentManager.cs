﻿using System;
using Bkm.Payment.Exception;

namespace Bkm.Payment.Domain
{
    public class PaymentManager : IPaymentManager
    {
        private readonly IPaymentRepository _paymentRepository;

        public PaymentManager(IPaymentRepository paymentRepository){
            _paymentRepository = paymentRepository;
        }

        public PaymentBase GetPayment(Guid paymentId)
        {
            var payment=_paymentRepository.Get(paymentId.ToString());
            if(payment==null){
                throw new BusinessRuleException(ErrorCode.PaymentNotFound, "PaymentNotFound");
            }
            return payment;
        }

        public string SavePayment(PaymentBase payment)
        {
            _paymentRepository.Insert(payment);
            return payment.ID;
        }

    }
}
