﻿using System;
using System.Collections.Generic;

namespace Bkm.Payment.Domain
{
    public interface IRepository<T> where T : ModelBase
    {       
        IEnumerable<T> GetAll();
        T Get(string id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
