﻿using System;
namespace Bkm.Payment.Domain
{
    public enum Currency
    {
        TRY = 1,
        BTC = 2,
        ETH = 3
    }
}
