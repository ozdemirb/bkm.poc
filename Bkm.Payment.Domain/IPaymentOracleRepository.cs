﻿using System;
namespace Bkm.Payment.Domain
{
    public interface IPaymentOracleRepository
    {
        void SavePayment(PaymentBase payment);
    }
}
