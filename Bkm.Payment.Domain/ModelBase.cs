﻿using System;
namespace Bkm.Payment.Domain
{

    public class ModelBase
    {
        public string ID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ModifyDate { get; set; }
    }
}
