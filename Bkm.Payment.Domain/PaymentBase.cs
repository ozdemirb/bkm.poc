﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bkm.Payment.Domain
{
    [Table("Payment")]
    public class PaymentBase:ModelBase
    {
        public decimal Amount { get; set; }

        public int Bank { get; set; }

        public string Currency { get; set; }
    }
}
