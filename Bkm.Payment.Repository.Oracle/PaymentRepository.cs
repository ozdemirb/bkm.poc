﻿using System;
using System.Data.OracleClient;
using Bkm.Payment.Domain;

namespace Bkm.Payment.Repository.Oracle
{
    public class PaymentRepository : IPaymentOracleRepository
    {
        public void SavePayment(PaymentBase payment)
        {
            var connectionString = "Data Source = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = paymentpoc.cf1od9nt0c7n.us-east-2.rds.amazonaws.com)(PORT = 1521))(CONNECT_DATA = (SERVICE_NAME = ORCL))); User ID = payment; Password = payment12345";

            var dbCon = new OracleConnection(connectionString);
            try{
                dbCon.Open();

                OracleCommand cmd = new OracleCommand(@"INSERT INTO PAYMENT (BANK,AMOUNT,CURRENCY) VALUES(:BANK,:AMOUNT,CURRENCY)", dbCon);

                cmd.Parameters.Add(new OracleParameter()
                {
                    DbType = System.Data.DbType.String,
                    ParameterName = "BANK",
                    Value = payment.Bank
                });
                cmd.Parameters.Add(new OracleParameter()
                {
                    DbType = System.Data.DbType.String,
                    ParameterName = "AMOUNT",
                    Value = payment.Amount
                });
                cmd.Parameters.Add(new OracleParameter()
                {
                    DbType = System.Data.DbType.String,
                    ParameterName = "CURRENCY",
                    Value = payment.Currency
                });
                cmd.ExecuteNonQuery();
            }
            catch(System.Exception ex){
                Console.WriteLine(ex.ToString());
            }
           
        }
    }
}
