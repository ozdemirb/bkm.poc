﻿using System;
using Bkm.Payment.Domain;
using Bkm.Payment.Dto.Request;
using Bkm.Payment.Dto.Response;

namespace Bkm.Payment.ApplicationService
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentManager _paymentManager;
        public PaymentService(IPaymentManager paymentManager){
            _paymentManager = paymentManager;
        }

        public GetPaymentResponse GetPayment(Guid paymentId)
        {
            var payment=_paymentManager.GetPayment(paymentId);

            return new GetPaymentResponse()
            {
                Amount = payment.Amount,
                Currency = payment.Currency,
                Bank = payment.Bank
            };
        }

        public PaymentPayResponse Pay(PaymentPayRequest request)
        {
            var payment = new PaymentBase()
            {
                Bank=request.Bank,
                Amount=request.Amount,
                Currency= request.Currency,
                CreateDate=DateTime.Now
            };
            _paymentManager.SavePayment(payment);
            return new PaymentPayResponse()
            {
                PaymentId =Guid.Parse(payment.ID) 
            };
        }
    }
}
