﻿using System;
using Bkm.Payment.Domain;
using Bkm.Payment.Dto.Request;
using Bkm.Payment.Dto.Response;

namespace Bkm.Payment.ApplicationService
{
    public interface IPaymentService
    {
        PaymentPayResponse Pay(PaymentPayRequest request);

        GetPaymentResponse GetPayment(Guid paymentId);
    }
}
