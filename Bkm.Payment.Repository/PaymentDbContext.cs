﻿using System;
using Bkm.Payment.Repository.Map;
using Microsoft.EntityFrameworkCore;
using Bkm.Payment.Domain;
using System.Linq;

namespace Bkm.Payment.Repository
{
    public class PaymentDbContext : DbContext
    {
        public PaymentDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new PaymentMap(modelBuilder.Entity<PaymentBase>());
          
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => (x.State == EntityState.Added));

            foreach (var entry in modifiedEntries)
            {
                var entity = entry.Entity as ModelBase;
                entity.CreateDate = DateTime.Now;
            }
            return base.SaveChanges();
        }
    }
}
