﻿using System;
using Bkm.Payment.Domain;

namespace Bkm.Payment.Repository
{
    public class PaymentRepository : Repository<PaymentBase>, IPaymentRepository
    {
        public PaymentRepository(PaymentDbContext context) : base(context)
        {
        }
    }
}

