﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bkm.Payment.Repository.Map
{
    public class PaymentMap
    {
        public PaymentMap(EntityTypeBuilder<Domain.PaymentBase> entityBuilder)
        {
            entityBuilder.Ignore(t => t.CreateDate);
            entityBuilder.HasKey(t => t.ID);
            entityBuilder.Property(t => t.Currency).IsRequired();
            entityBuilder.Property(t => t.Amount).IsRequired();
            entityBuilder.Property(t => t.Bank).IsRequired();
            entityBuilder.Ignore(t=>t.ModifyDate);
                         


            //entityBuilder.Property(t => t.Email).IsRequired();
            //entityBuilder.Property(t => t.Password).IsRequired();
            //entityBuilder.Property(t => t.CitizenshipNumber).IsRequired();
            //entityBuilder.Property(t => t.DateOfBirth).IsRequired();
            //entityBuilder.Property(t => t.MobilePhone).IsRequired();
        }
    }
}
