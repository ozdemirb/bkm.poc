﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bkm.Payment.Domain;
using Microsoft.EntityFrameworkCore;

namespace Bkm.Payment.Repository
{
    public class Repository<T> : IRepository<T> where T : ModelBase
    {
        private readonly PaymentDbContext context;
        private DbSet<T> entities;
        string errorMessage = string.Empty;

        public Repository(PaymentDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }

        public T Get(string id)
        {
            return entities.SingleOrDefault(s => s.ID == id);
        }
        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.SaveChanges();
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            context.SaveChanges();
        }
    }
}
