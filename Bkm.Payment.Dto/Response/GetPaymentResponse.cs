﻿using System;
namespace Bkm.Payment.Dto.Response
{
    public class GetPaymentResponse:ResponseBase    
    {
        public decimal Amount { get; set; }

        public int Bank { get; set; }

        public string Currency { get; set; } 
    }
}
