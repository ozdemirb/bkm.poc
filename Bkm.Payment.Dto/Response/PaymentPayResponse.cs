﻿using System;
namespace Bkm.Payment.Dto.Response
{
    public class PaymentPayResponse:ResponseBase
    {
        public Guid PaymentId { get; set; }
    }
}
    