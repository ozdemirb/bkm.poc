﻿using System;
namespace Bkm.Payment.Dto.Response
{
    public class ResponseBase
    {
        public ResponseBase()
        {
            Success = true;
        }
        public bool Success { get; set; }

        public string Message { get; set; }

        public string UserMessage { get; set; }

        public int Code { get; set; }
    }
}
