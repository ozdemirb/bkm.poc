﻿using System;
namespace Bkm.Payment.Dto.Request
{
    public class PaymentPayRequest
    {
        public decimal Amount { get; set; }

        public int Bank { get; set; }

        public string Currency { get; set; }
    }
}
