﻿using System;
namespace Bkm.Payment.Exception
{
    public enum ErrorCode
    {
        InternalServerError = 1000,
        PaymentNotFound = 2000
    }
}
