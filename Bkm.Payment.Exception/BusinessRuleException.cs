﻿using System;

namespace Bkm.Payment.Exception
{
    public class BusinessRuleException : System.Exception
    {
        public ErrorCode Code { get; set; }
        public new string Message { get; set; }
        private bool IgnoreRollback { get; set; }
        public string UserMessage { get; set; }
        private int MessageCode { get; set; }

        public BusinessRuleException() : base()
        {

        }
        public BusinessRuleException(int messageCode) : base()
        {
            MessageCode = messageCode;
        }

        public BusinessRuleException(ErrorCode code, string message, string userMessage, bool ignoreRollback)
            : base(message)
        {
            Code = code;
            Message = message;
            IgnoreRollback = ignoreRollback;
            UserMessage = userMessage;
        }

        public BusinessRuleException(ErrorCode errorCode, string message)
            : base(message)
        {
            Code = errorCode;
            Message = message;
            UserMessage = message;
        }

        public BusinessRuleException(ErrorCode code, string message, string userMessage)
            : base(message)
        {
            Code = code;
            Message = message;
            UserMessage = userMessage;
        }

        public BusinessRuleException(string message) : base(message)
        {
        }
    }
}
